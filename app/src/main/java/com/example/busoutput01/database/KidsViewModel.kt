package com.example.busoutput01.database

import androidx.lifecycle.*
import com.example.busoutput01.database.room.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 *  ********************  WholeKidsViewModel  ********************
 */
// ViewModel を拡張して、KidsRepository をパラメータとして取得する KidsViewModel というクラスを作成
class WholeKidsViewModel(private val repository: WholeKidsRepository) : ViewModel() {

    val allWholeKids: LiveData<List<WholeKids>> = repository.allWholeKids.asLiveData()

    fun insertWholeKids(kids: WholeKids) = viewModelScope.launch {
        repository.insertWholeKids(kids)
    }
    fun updateWholeKids(kids: WholeKids) = viewModelScope.launch {
        repository.updateWholeKids(kids)
    }

    fun deleteAllWholeKids() = viewModelScope.launch {
        repository.deleteAllWholeKids()
    }

    // ID -> Name
    fun getNameFromId(id: Int) = repository.getNameFromId(id)

}

class WholeKidsViewModelFactory(private val repository: WholeKidsRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WholeKidsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return WholeKidsViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

/**
 *  ********************  RideIdViewModel  ********************
 */
// ViewModel を拡張して、KidsRepository をパラメータとして取得する KidsViewModel というクラスを作成
class RideIdViewModel(private val repository: RideIdRepository) : ViewModel() {

    val allRideId: LiveData<List<RideKidsId>> = repository.allRideId.asLiveData()

    fun insertRideId(kids: RideKidsId) = viewModelScope.launch {
        repository.insertRideId(kids)
    }
    fun updateRideId(kids: RideKidsId) = viewModelScope.launch(Dispatchers.IO) {
        repository.updateRideId(kids)
    }
    fun deleteAllRideId() = viewModelScope.launch {
        repository.deleteAllRideId()
    }

    // *** Board Now *** //
    fun updateRideIdNow() = viewModelScope.launch(Dispatchers.IO) {
        repository.updateRideIdNow()
    }
    // *** Deboard *** //
//    fun updateBoardtoDeboard(kids: RideKidsId) = viewModelScope.launch(Dispatchers.IO) {
//        repository.updateBoardtoDeboard(kids)
//    }
    fun updateDeboarded() = viewModelScope.launch(Dispatchers.IO) {
        repository.updateDeboarded()
    }
}

class RideIdViewModelFactory(private val repository: RideIdRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(RideIdViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return RideIdViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
/**
 *  ********************  RideViewViewModel  ********************
 */
// ViewModel を拡張して、KidsRepository をパラメータとして取得する KidsViewModel というクラスを作成
class RideViewViewModel(private val repository: RideViewRepository) : ViewModel() {

    val allRideView: LiveData<List<RideKidsView>> = repository.allRideView.asLiveData()

    suspend fun findById(id:Int) = viewModelScope.launch {
        repository.findById(id)
    }


    val getBoardNew: LiveData<List<RideKidsView>> = repository.getBoardNew.asLiveData()

    val getBoardRollCall: LiveData<List<RideKidsView>> = repository.getBoardRollCall.asLiveData()

    val getDeboardList: LiveData<List<RideKidsView>> = repository.getDeboardList.asLiveData()

    val getTodayLarge: LiveData<List<RideKidsView>> = repository.getTodayLarge.asLiveData()
    val getTodaySmall: LiveData<List<RideKidsView>> = repository.getTodaySmall.asLiveData()
}

class RideViewViewModelFactory(private val repository: RideViewRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(RideViewViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return RideViewViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

/**
 *  ********************  NotRidingViewViewModel  ********************
 */
// ViewModel を拡張して、KidsRepository をパラメータとして取得する KidsViewModel というクラスを作成
class NotRidingViewViewModel(private val repository: NotRidingViewRepository) : ViewModel() {

    val allNotRidingView: LiveData<List<NotRidingView>> = repository.allNotRidingView.asLiveData()
    val largeNotRidingView: LiveData<List<NotRidingView>> = repository.largeNotRidingView.asLiveData()
    val smallNotRidingView: LiveData<List<NotRidingView>> = repository.smallNotRidingView.asLiveData()

    suspend fun countAbsenteeLargeR() = viewModelScope.launch {
        repository.countAbsenteeLargeR()
    }
    suspend fun countAbsenteeSmallR() = viewModelScope.launch {
        repository.countAbsenteeSmallR()
    }

}

class NotRidingViewModelFactory(private val repository: NotRidingViewRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NotRidingViewViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return NotRidingViewViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
