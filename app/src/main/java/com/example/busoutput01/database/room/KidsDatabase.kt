package com.example.busoutput01.database.room

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = arrayOf(WholeKids::class, RideKidsId::class),
    views = arrayOf(RideKidsView::class, NotRidingView::class),
    version = 1,
    exportSchema = false
)
abstract class KidsRoomDatabase : RoomDatabase() {

    abstract fun wholeKidsDao(): WholeKidsDao
    abstract fun rideIdDao(): RideIdDao
    abstract fun rideKidsViewDao(): RideViewDao
    abstract fun notRidingViewDao(): NotRidingViewDao

    //でコールバックを作成する
    private class RideKidsDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {

        /**
         * データベースにデータを入力
         */
        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {
                    var wholeKidsDao = database.wholeKidsDao()
                    var rideIdDao = database.rideIdDao()

                    // Delete all content here.
//                    wholeKidsDao.deleteAllWholeKids()
                    rideIdDao.deleteAllRideId()

                    // Add wholeKids data
                    rideIdDao.insertRideId(RideKidsId(100,1,1))
                    rideIdDao.insertRideId(RideKidsId(101,1,1))
                    rideIdDao.insertRideId(RideKidsId(102,1,1))
                    rideIdDao.insertRideId(RideKidsId(103,1,1))
                    rideIdDao.insertRideId(RideKidsId(104,1,1))
                    rideIdDao.insertRideId(RideKidsId(105,1,1))
                    rideIdDao.insertRideId(RideKidsId(106,1,1))
                    rideIdDao.insertRideId(RideKidsId(107,1,1))
//                    rideIdDao.insertRideId(RideKidsId(108,0,1))
//                    rideIdDao.insertRideId(RideKidsId(109,1,0))
//                    rideIdDao.insertRideId(RideKidsId(110,0,0))


                }
            }
        }

    }

    companion object {
        // シングルトンは、複数のデータベースのインスタンスが同時に開くのを防ぐ
        @Volatile
        private var INSTANCE: KidsRoomDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): KidsRoomDatabase {

            // INSTANCE が NULL ならば、データベースを作成する
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    KidsRoomDatabase::class.java,
                    "kids_database"
                )

                    // asset フォルダの中のDBを取り込む
                    .createFromAsset("whole_kids_table")

                    // Version違ってMigrationなかったら消しちゃう
                    // https://qiita.com/satohu20xx/items/071570f7b4766b3124b7
//                    .fallbackToDestructiveMigration()

                    .addCallback(RideKidsDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }
}