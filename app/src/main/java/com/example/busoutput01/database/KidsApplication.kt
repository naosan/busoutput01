package com.example.busoutput01.database

import android.app.Application
import com.example.busoutput01.database.room.KidsRoomDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

// データベースとリポジトリのインスタンスは、それぞれ 1 つのみにする必要があります。
// そのため、Application を拡張した KidsApplication というクラスを新たに作成します
class KidsApplication : Application() {

    // No need to cancel this scope as it'll be torn down with the process
    val applicationScope = CoroutineScope(SupervisorJob())

    // アプリケーションの起動時ではなくデータベースとリポジトリが必要なときだけ作成されるように、遅延で使用する
    val database by lazy { KidsRoomDatabase.getDatabase(this, applicationScope) }

    val wholekids_repository by lazy { WholeKidsRepository(database.wholeKidsDao()) }
    val rideid_repository by lazy { RideIdRepository(database.rideIdDao()) }
    val rideview_repository by lazy { RideViewRepository(database.rideKidsViewDao()) }
    val notview_repository by lazy { NotRidingViewRepository(database.notRidingViewDao()) }
}