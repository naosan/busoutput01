package com.example.busoutput01.database.room

import androidx.room.ColumnInfo
import androidx.room.DatabaseView
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 *  ********************  WholeKids  ********************
 */
@Entity(tableName = "whole_kids_table")
data class WholeKids(
    @PrimaryKey(autoGenerate = false) @ColumnInfo(name = "whole_kids_id") val wholeKidsId: Int,
    @ColumnInfo(name = "bus_course") val busCourse: Int,
    @ColumnInfo(name = "k_year") val kYear: Int,
    @ColumnInfo(name = "k_class") val kClass: String,
    @ColumnInfo(name = "last_name") val lastName: String,
    @ColumnInfo(name = "first_name") val firstName: String
)
/**
 *  ********************  RideKidsId  ********************
 */
@Entity(tableName = "ride_id_table")
data class RideKidsId(
    @PrimaryKey(autoGenerate = false) @ColumnInfo(name = "ride_kids_id") val rideKidsId: Int,
    @ColumnInfo(name = "ride_flag") val rideFlag: Int,
    @ColumnInfo(name = "now_flag") val nowFlag: Int
)

/**
 *  ********************  RideKidsView  ********************
 */
@DatabaseView(
    viewName = "ride_kids_view",
    value = " SELECT whole_kids_table.whole_kids_id AS rideViewId, " +
            "whole_kids_table.bus_course AS busCourse, " +
            "whole_kids_table.k_year AS kYear, " +
            "whole_kids_table.k_class AS kClass, " +
            "whole_kids_table.last_name AS lastName, " +
            "whole_kids_table.first_name AS firstName, " +
            "ride_id_table.ride_flag AS rideFlag, " +
            "ride_id_table.now_flag AS nowFlag " +
            "FROM whole_kids_table " +
            "INNER JOIN ride_id_table " +
            "ON whole_kids_table.whole_kids_id = ride_id_table.ride_kids_id"
)
data class RideKidsView(
    val rideViewId: Int,
    val busCourse: Int,
    val kYear: Int,
    val kClass: String,
    val lastName: String,
    val firstName: String,
    val rideFlag: Int,
    val nowFlag: Int
)

/**
 *  ********************  NotRidingView  ********************
 */
@DatabaseView(
    viewName = "not_riding_view",
    value = " SELECT whole_kids_table.whole_kids_id AS notViewId, " +
            "whole_kids_table.bus_course AS busCourse, " +
            "whole_kids_table.k_year AS kYear, " +
            "whole_kids_table.k_class AS kClass, " +
            "whole_kids_table.last_name AS lastName, " +
            "whole_kids_table.first_name AS firstName " +
            "FROM whole_kids_table " +
            "LEFT OUTER JOIN ride_id_table " +
            "ON whole_kids_table.whole_kids_id = ride_id_table.ride_kids_id " +
            "WHERE ride_id_table.ride_kids_id IS NULL"
)
data class NotRidingView(
    val notViewId: Int,
    val busCourse: Int,
    val kYear: Int,
    val kClass: String,
    val lastName: String,
    val firstName: String
)
