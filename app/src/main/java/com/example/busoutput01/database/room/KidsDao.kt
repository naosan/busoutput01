package com.example.busoutput01.database.room

import androidx.room.*
import kotlinx.coroutines.flow.Flow


/**
 *  ********************  WholeKidsDao  ********************
 */
@Dao
interface WholeKidsDao {

    @Query("SELECT * FROM whole_kids_table ORDER BY bus_course ASC, k_year ASC, k_class")
    fun getWholeKids(): Flow<List<WholeKids>>

    // *** Option Whole Kids *** //
    // 1.コンフリクトが発生すると挿入が実行されず-1が返る
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertWholeKids(kids: WholeKids)

    // 2.データが重複した場合でも自動でupdateにしてくれる
    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateWholeKids(kids: WholeKids)

    // *** POST *** //
    @Query("DELETE FROM whole_kids_table")
    fun deleteAllWholeKids()

    // *** SharedPreferences *** //
    // 大型バスの子カウント
    @Query("SELECT COUNT(*) FROM whole_kids_table where bus_course = 1 or bus_course = 2 or  bus_course = 3")
    fun countWholeLarge():Int

    // 小型バスの子カウント
    @Query("SELECT COUNT(*) FROM whole_kids_table where bus_course = 4 or bus_course = 5 or  bus_course = 6")
    fun countWholeSmall():Int

    // 全員カウント
    @Query("SELECT COUNT(*) FROM whole_kids_table")
    fun countWholeAll():Int


    // *** Board Scan *** //
    // ID から名前を取得
    @Query("SELECT * from whole_kids_table WHERE whole_kids_id = :id")
    fun getNameFromId(id: Int): Flow<WholeKids>
}
/**
 *  ********************  RideIdDao  ********************
 */
@Dao
interface RideIdDao {

    @Query("SELECT * FROM ride_id_table ORDER BY ride_kids_id ASC")
    fun getRideId(): Flow<List<RideKidsId>>

    // *** Board Scan *** //
    // 1.コンフリクトが発生すると挿入が実行されず-1が返る
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertRideId(kids: RideKidsId)

    // 2.データが重複した場合でも自動でupdateにしてくれる
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateRideId(kids: RideKidsId)

    // *** Option Inquiry *** //
    @Query("DELETE FROM ride_id_table")
    fun deleteAllRideId()

    // *** Board Now *** //
    // now_flag = 1 -> 0
    @Query("UPDATE ride_id_table SET now_flag = 0 WHERE now_flag = 1")
    suspend fun updateRideIdNow()

    // 今乗った子カウント
    @Query("SELECT COUNT(*) FROM ride_id_table WHERE now_flag = 1")
    fun countBoardNow():Int
    // 乗車している子カウント
    @Query("SELECT COUNT(*) FROM ride_id_table WHERE ride_flag = 1")
    fun countBoardRollCall():Int


    // ride_flag = 0 -> 2
    @Query("UPDATE ride_id_table SET ride_flag = 2 WHERE ride_flag = 0")
    suspend fun updateDeboarded()

    // 降車した子カウント
    @Query("SELECT COUNT(*) FROM ride_id_table WHERE ride_flag = 0")
    fun countDeboarded():Int
    // 降車していない子カウント
    @Query("SELECT COUNT(*) FROM ride_id_table WHERE ride_flag = 1")
    fun countDeboardBoarding():Int

}

/**
 *  ********************  RideKidsViewDao  ********************
 */
@Dao
interface RideViewDao {

    @Query("select * from ride_kids_view")
    fun getRideViewAll(): Flow<List<RideKidsView>>

    @Query("select * from ride_kids_view where rideViewId = :id")
    suspend fun findById(id: Int): RideKidsView

    // *** Board Now *** //
    // 今乗車した子一覧
    @Query("select * from ride_kids_view WHERE nowFlag = 1 ORDER BY kYear ASC, kClass ASC")
    fun getBoardNew(): Flow<List<RideKidsView>>

    // *** Board RollCall *** //
    // 点呼（乗車している子一覧）
    @Query("select * from ride_kids_view WHERE rideFlag = 1 ORDER BY kYear ASC, kClass ASC")
    fun getBoardRollCall(): Flow<List<RideKidsView>>

    // *** Deboard *** //
    // 降車した子一覧
    @Query("select * from ride_kids_view WHERE rideFlag < 2 ORDER BY rideFlag DESC, kYear ASC, kClass ASC")
    fun getDeboardList(): Flow<List<RideKidsView>>

    // *** Today Boarded *** //

    // 今日バスに乗った子（大型バス）
    @Query("select * from ride_kids_view where busCourse = 1 or busCourse = 2 or  busCourse = 3 ORDER BY busCourse ASC, kYear ASC, kClass ASC")
    fun getTodayLarge(): Flow<List<RideKidsView>>

    // 今日バスに乗った子（小型バス）
    @Query("select * from ride_kids_view where busCourse = 4 or busCourse = 5 or  busCourse = 6 ORDER BY busCourse ASC, kYear ASC, kClass ASC")
    fun getTodaySmall(): Flow<List<RideKidsView>>

    // 今日乗車した子カウント（大型バス）
    @Query("SELECT COUNT(*) FROM ride_kids_view where busCourse = 1 or busCourse = 2 or  busCourse = 3 ORDER BY busCourse, kYear ASC, kClass ASC")
    fun countTodayLarge(): Int

    // 今日乗車した子カウント（小型バス）
    @Query("SELECT COUNT(*) FROM ride_kids_view where busCourse = 4 or busCourse = 5 or  busCourse = 6 ORDER BY busCourse, kYear ASC, kClass ASC")
    fun countTodaySmall(): Int

}
/**
 *  ********************  NotRidingViewDao  ********************
 */
@Dao
interface NotRidingViewDao {

    // *** Absentee *** //
    @Query("select * from not_riding_view ORDER BY kYear ASC, kClass ASC, lastName ASC")
    fun getNotRidingViewAll(): Flow<List<NotRidingView>>


    @Query("select * from not_riding_view where busCourse = 1 or busCourse = 2 or  busCourse = 3 ORDER BY kYear ASC, kClass ASC, busCourse ASC,lastName ASC")
    fun getNotRidingViewLarge(): Flow<List<NotRidingView>>

    @Query("select * from not_riding_view where busCourse = 4 or busCourse = 5 or  busCourse = 6 ORDER BY kYear ASC, kClass ASC, busCourse ASC, lastName ASC")
    fun getNotRidingViewSmall(): Flow<List<NotRidingView>>

    // *** Count *** //
    @Query("SELECT COUNT(*) FROM not_riding_view where busCourse = 1 or busCourse = 2 or  busCourse = 3")
    suspend fun countAbsenteeLarge():Int

    @Query("SELECT COUNT(*) FROM not_riding_view where busCourse = 4 or busCourse = 5 or  busCourse = 6")
    suspend fun countAbsenteeSmall():Int
}