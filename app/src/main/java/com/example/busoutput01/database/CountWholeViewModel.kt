package com.example.busoutput01.database

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.busoutput01.database.room.NotRidingViewDao
import com.example.busoutput01.database.room.RideIdDao
import com.example.busoutput01.database.room.RideViewDao
import com.example.busoutput01.database.room.WholeKidsDao
import kotlinx.coroutines.launch

/**
 *  ********************  WholeKidsViewModel  ********************
 */
class CountWholeViewModel(private val wholeKidsDao: WholeKidsDao) : ViewModel() {

    /**
     * Retrieve an item from the dao.
     */
    fun countWholeLarge(): Int {
        return wholeKidsDao.countWholeLarge()
    }
    fun countWholeSmall(): Int {
        return wholeKidsDao.countWholeSmall()
    }
    fun countWholeAll(): Int {
        return wholeKidsDao.countWholeAll()
    }

}
/**
 * Factory class to instantiate the [ViewModel] instance.
 */
class CountWholeViewModelFactory(private val wholeKidsDao: WholeKidsDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CountWholeViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CountWholeViewModel(wholeKidsDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

/**
 * View Model to keep a reference to the Inventory repository and an up-to-date list of all items.
 *
 */
/**
 *  ********************  RideIdViewModel  ********************
 */
class CountViewModel(private val rideIdDao: RideIdDao) : ViewModel() {

    /**
     * Retrieve an item from the dao.
     */
    fun countBoardNow(): Int {
        return rideIdDao.countBoardNow()
    }
    fun countBoardRollCall(): Int {
        return rideIdDao.countBoardRollCall()
    }
    fun countDeboarded(): Int {
        return rideIdDao.countDeboarded()
    }
    fun countDeboardBoarding(): Int {
        return rideIdDao.countDeboardBoarding()
    }

}
/**
 * Factory class to instantiate the [ViewModel] instance.
 */
class CountViewModelFactory(private val rideIdDao: RideIdDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CountViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CountViewModel(rideIdDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

/**
 *  ********************  RideViewViewModel  ********************
 */
class CountViewViewModel(private val rideViewDao: RideViewDao) : ViewModel() {

    /**
     * Retrieve an item from the dao.
     */
    fun countTodayLarge(): Int {
        return rideViewDao.countTodayLarge()
    }
    fun countTodaySmall(): Int {
        return rideViewDao.countTodaySmall()
    }

}
/**
 * Factory class to instantiate the [ViewModel] instance.
 */
class CountViewViewModelFactory(private val rideViewDao: RideViewDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CountViewViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CountViewViewModel(rideViewDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}




/**
 *  ********************  NotRidingViewViewModel  ********************
 */
class CountNotViewModel(private val notRidingViewDao: NotRidingViewDao) : ViewModel() {

    /**
     * Retrieve an item from the dao.
     */
    suspend fun countAbsenteeLarge(): Int {
        return notRidingViewDao.countAbsenteeLarge()
    }
    suspend fun countAbsenteeSmall(): Int {
        return notRidingViewDao.countAbsenteeSmall()
    }

}
/**
 * Factory class to instantiate the [ViewModel] instance.
 */
class CountNotViewModelFactory(private val notRidingViewDao: NotRidingViewDao) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CountNotViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CountNotViewModel(notRidingViewDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
