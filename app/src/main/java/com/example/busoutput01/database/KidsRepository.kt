package com.example.busoutput01.database

import android.content.Context
import android.util.Log
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import com.example.busoutput01.database.room.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow


// リポジトリ コンストラクタには、データベース全体ではなく DAO が渡されます。
// コンストラクタでDAOをプライベートプロパティとして宣言します
/**
 *  ********************  WholeKidsRepository  ********************
 */
class WholeKidsRepository(private val wholeKidsDao: WholeKidsDao) {

    val allWholeKids: Flow<List<WholeKids>> = wholeKidsDao.getWholeKids()

    //　Room は、メインスレッド以外で suspend クエリを実行します。
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertWholeKids(kids: WholeKids) {
        wholeKidsDao.insertWholeKids(kids)
    }
    suspend fun updateWholeKids(kids: WholeKids) {
        wholeKidsDao.updateWholeKids(kids)
    }

    // ID -> Name
    fun getNameFromId(id: Int): LiveData<WholeKids> {
        return wholeKidsDao.getNameFromId(id).asLiveData()
    }
     fun deleteAllWholeKids() {
        wholeKidsDao.deleteAllWholeKids()
    }

}
/**
 *  ********************  RideIdRepository  ********************
 */
class RideIdRepository(private val rideIdDao: RideIdDao) {

    val allRideId: Flow<List<RideKidsId>> = rideIdDao.getRideId()

    //　Room は、メインスレッド以外で suspend クエリを実行します。
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertRideId(kids: RideKidsId) {
        rideIdDao.insertRideId(kids)
    }
    fun updateRideId(kids: RideKidsId) {
        rideIdDao.updateRideId(kids)
    }
    fun deleteAllRideId() {
        rideIdDao.deleteAllRideId()
    }
    // *** Board Now *** //
    suspend fun updateRideIdNow() {
        rideIdDao.updateRideIdNow()
    }
    // *** Deboard *** //

    //    suspend fun updateBoardtoDeboard(kids: RideKidsId) {
//        rideIdDao.updateBoardtoDeboard(kids)
//    }
    suspend fun updateDeboarded() {
        rideIdDao.updateDeboarded()
    }

}

/**
 *  ********************  RideKidsViewRepository  ********************
 */
class RideViewRepository(private val rideViewDao: RideViewDao) {

    // Room は、すべてのクエリを個別のスレッドで実行します。
    // Room から単語の Flow リストを取得することで初期化されます。
    val allRideView: Flow<List<RideKidsView>> = rideViewDao.getRideViewAll()

    //　Room は、メインスレッド以外で suspend クエリを実行します。
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun findById(id: Int) {
        rideViewDao.findById(id)
    }

    val getBoardNew: Flow<List<RideKidsView>> = rideViewDao.getBoardNew()

    val getBoardRollCall: Flow<List<RideKidsView>> = rideViewDao.getBoardRollCall()

    val getDeboardList: Flow<List<RideKidsView>> = rideViewDao.getDeboardList()

    val getTodayLarge: Flow<List<RideKidsView>> = rideViewDao.getTodayLarge()
    val getTodaySmall: Flow<List<RideKidsView>> = rideViewDao.getTodaySmall()

}

/**
 *  ********************  NotRidingViewRepository  ********************
 */
class NotRidingViewRepository(private val notRidingViewDao: NotRidingViewDao) {

    val allNotRidingView: Flow<List<NotRidingView>> = notRidingViewDao.getNotRidingViewAll()
    val largeNotRidingView: Flow<List<NotRidingView>> = notRidingViewDao.getNotRidingViewLarge()
    val smallNotRidingView: Flow<List<NotRidingView>> = notRidingViewDao.getNotRidingViewSmall()

    @WorkerThread
    suspend fun countAbsenteeLargeR(): Int {
        return notRidingViewDao.countAbsenteeLarge()
    }
    @WorkerThread
    suspend fun countAbsenteeSmallR(): Int {
        return notRidingViewDao.countAbsenteeSmall()
    }

}