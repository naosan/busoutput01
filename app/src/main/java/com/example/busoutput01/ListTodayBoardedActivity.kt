package com.example.busoutput01

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.busoutput01.database.*
import com.example.busoutput01.databinding.ActivityListTodayBoardedBinding
import kotlinx.android.synthetic.main.activity_list_today_boarded.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ListTodayBoardedActivity : AppCompatActivity() {

    private lateinit var binding: ActivityListTodayBoardedBinding

    // RideViewViewModel
    private val viewViewModel: RideViewViewModel by viewModels {
        RideViewViewModelFactory((application as KidsApplication).rideview_repository)
    }
    // Count
    private val countViewViewModel: CountViewViewModel by viewModels {
        CountViewViewModelFactory((application as KidsApplication).database.rideKidsViewDao())
    }

    private lateinit var viewTodayAdapter: ListTodayBoardedAdapter

    // SharedPreferences
    private lateinit var sharedPref: SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityListTodayBoardedBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        // Recyclerview 表示
        val recyclerviewBoardRollCall = findViewById<RecyclerView>(R.id.recyclerviewToday)
        val adapterBoardRollCall = ListTodayBoardedAdapter(this, this.viewViewModel).also {
            viewTodayAdapter = it
        }
        recyclerviewBoardRollCall.adapter = adapterBoardRollCall
        recyclerviewBoardRollCall.layoutManager = LinearLayoutManager(this)

        // *** SharedPreferences *** //
        sharedPref = getSharedPreferences("com.example.busoutput01", Context.MODE_PRIVATE)
        val cntSL: Int = sharedPref.getInt("wholeLarge", 0)
        val cntSS: Int = sharedPref.getInt("wholeSmall", 0)

        // 大型バス
        binding.btnLarge.setOnClickListener {
            binding.textTodayCount.text = ""
            binding.btnLarge.setBackgroundColor((Color.parseColor("#f1981c")))
            binding.btnSmall.setBackgroundColor((Color.parseColor("#AA999999")))
            viewViewModel.getTodayLarge.observe( this) { kids ->
                kids.let { adapterBoardRollCall.submitList(it) }
            }
            // count 表示
            var cntL = 0
            CoroutineScope(Dispatchers.IO).launch {
                cntL = countViewViewModel.countTodayLarge()
            }
            Handler(Looper.getMainLooper()).postDelayed({
                binding.textTodayCount.text = cntL.toString() + " 人 / (" + cntSL.toString() + "人)"
            }, 300)

        }
        // 小型バス
        binding.btnSmall.setOnClickListener {
            binding.textTodayCount.text = ""
            binding.btnSmall.setBackgroundColor((Color.parseColor("#f1981c")))
            binding.btnLarge.setBackgroundColor((Color.parseColor("#AA999999")))
            viewViewModel.getTodaySmall.observe( this) { kids ->
                kids.let { adapterBoardRollCall.submitList(it) }
            }

            // count 表示
            var cntS = 0
            CoroutineScope(Dispatchers.IO).launch {
                cntS = countViewViewModel.countTodaySmall()
            }
            Handler(Looper.getMainLooper()).postDelayed({
                binding.textTodayCount.text = cntS.toString() + " 人 / (" + cntSS.toString() + "人)"
            }, 300)

        }

        // 下線
        val dividerItemDecoration = DividerItemDecoration(this, LinearLayoutManager(this).getOrientation())
        recyclerviewToday.addItemDecoration(dividerItemDecoration)





        binding.btnTodayMain.setOnClickListener {
            val intent = Intent(application, MainActivity::class.java)
            startActivity(intent)
        }


    }
}