package com.example.busoutput01

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.core.app.AppLaunchChecker
import com.example.busoutput01.database.*
import com.example.busoutput01.database.room.RideKidsView
import com.example.busoutput01.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var sharedPref: SharedPreferences

    // RideViewViewModel
    private val rideViewViewModel: RideViewViewModel by viewModels {
        RideViewViewModelFactory((application as KidsApplication).rideview_repository)
    }
    // Count
    private val countWholeViewModel: CountWholeViewModel by viewModels {
        CountWholeViewModelFactory((application as KidsApplication).database.wholeKidsDao())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.btnMainBoarded.setOnClickListener {
            val intent = Intent(application, ListTodayBoardedActivity::class.java)
            startActivity(intent)
        }

        // 初回起動時
        val first = AppLaunchChecker.hasStartedFromLauncher(applicationContext)
        if (!first){

            // *** WholeKids Count ***
            WholeKidsCount()

            // 初回起動判定 はずす
            AppLaunchChecker.onActivityCreate(this)


        }

        binding.btnOutPut.setOnClickListener {


        }

        /**
         *  ********************  okhttp3  ********************
         */
        val client = OkHttpClient()

        binding.btnOutPut.setOnClickListener {

            val app = application as KidsApplication

            // Mainスレッド上でコルーチンを開始します
            CoroutineScope(Dispatchers.Main).launch {

                // IOスレッドで非同期処理を実行し、結果を待ちます
                val csvData = withContext(Dispatchers.IO) {
                    getKidsAsCSV(app.rideview_repository)
                }.trimIndent()

                // 非同期処理が終了した後に出力します
                Log.d("Log",csvData)

                val requestBody = csvData.toRequestBody("text/csv".toMediaTypeOrNull())

                val request = Request.Builder()
                    .url("[公開されたGoogle Apps ScriptのURL]")
    //               .header("Content-Type", "text/csv")

                    .post(requestBody)
                    .build()

                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        e.printStackTrace()
                    }

                    override fun onResponse(call: Call, response: Response) {

                        // トースト表示のためのUIスレッド実行
                        runOnUiThread {
                            if (response.isSuccessful) {
                                // レスポンスの処理
                                val responseData = response.body?.string()
                                Log.d("Log", responseData.toString())
                                Log.d("Log", "OK")
                                binding.textResult.text = responseData
                            } else {
                                // エラー処理
                                Log.d("Log", "Error: ${response.code}")
                                Log.d("Log", "Error")
                                binding.textResult.text = "Error: ${response.code}"
                            }
                        }
                    }
                })
            }

        }


    }


    /**
     *** convert DB to CSV ***
     */

    fun convertKidsToCSV(kidsList: List<RideKidsView>): String {
        // 日付
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val currentDate = dateFormat.format(Date())

        val stringBuilder = StringBuilder()
        // ヘッダーの追加
//        stringBuilder.append("kids_id,name\n")

        // 各レコードの追加
        for (kid in kidsList) {
            stringBuilder.append("${currentDate},${kid.rideViewId},${kid.kClass},${kid.lastName},${kid.firstName}\n")
        }
        return stringBuilder.toString()
    }
    suspend fun getKidsAsCSV(repository: RideViewRepository): String {
        // データベースからデータを取得
        val kidsList = repository.allRideView.first()

        // CSV形式のStringを返す
        return convertKidsToCSV(kidsList)
    }


    /**
     *** WholeKids Count ***
     */
    private fun WholeKidsCount(){

        // *** SharedPreferences *** //
        sharedPref = getSharedPreferences("com.example.busoutput01", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()

        // 大型バス
        CoroutineScope(Dispatchers.IO).launch {
            val cntL = countWholeViewModel.countWholeLarge()
            editor.putInt("wholeLarge", cntL).apply()
        }
        //小型バス
        CoroutineScope(Dispatchers.IO).launch {
            val cntS = countWholeViewModel.countWholeSmall()
            editor.putInt("wholeSmall", cntS).apply()
        }
    }


}