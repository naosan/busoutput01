package com.example.busoutput01

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.busoutput01.database.RideViewViewModel
import com.example.busoutput01.database.room.RideKidsView
import com.example.busoutput01.databinding.RecyclerviewItemTodayBinding

// KidsListAdapter は、onCreateViewHolder に KidsViewHolder を作成し、それを onBindViewHolder にバインドします
/**
 * RecyclerViewのListAdapterクラス
 *
 * 書式は
 * class クラス名 : ListAdapter<A,B>(C: DiffUtils.ItemCallback)
 * A:Object は表示するデータ
 * B:クラス内でViewHolderを定義しそのクラス名をセット
 * ViewHolderは2つの抽象メソッドでも使用する
 * C:DiffUtils.ItemCallback は A:Objectの差分確認方法を実装したItemCallback
 * クラス内にcompanion objectを作成しDiffUtil.ItemCallbackを作成
 * 2つの抽象メソッドが必要
 *
 * 画面遷移メソッドは呼出し元のフラグメント側で設定している
 */
class ListTodayBoardedAdapter(
    private val viewLifecycleOwner: LifecycleOwner,
    private val viewModel: RideViewViewModel
) : ListAdapter<RideKidsView, ListTodayBoardedAdapter.BoardRollCallViewHolder>(WordsComparator()) {

    //onCreateViewHolder→ViewHolderをリターン
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BoardRollCallViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return BoardRollCallViewHolder(RecyclerviewItemTodayBinding.inflate(layoutInflater, parent, false))
    }

    //onBindViewHolder→画面スクロール時に現在のposのデータをViewHolderに渡す
    // 引数の部分は画面遷移の関数を受取る(Item型を受取りUnit(void)を返す)
    override fun onBindViewHolder(holder: BoardRollCallViewHolder, position: Int) {
        // カレントデータの取得
        val current = getItem(position)
        holder.bind(current, viewLifecycleOwner, viewModel)
    }

    // テキストを TextView にバインドできます
    class BoardRollCallViewHolder(private val binding: RecyclerviewItemTodayBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(kids: RideKidsView, viewLifecycleOwner: LifecycleOwner, viewModel: RideViewViewModel) {
            binding.run {
                lifecycleOwner = viewLifecycleOwner
                // layout
//                word = kids

                // バスコース
                val bus_course = kids.busCourse
                when (bus_course) {
                    1 -> {
                        binding.textViewBusCourse.setText("青")
                        binding.textViewBusCourse.setTextColor(Color.parseColor("#1919FF"))
                    }
                    2 -> {
                        binding.textViewBusCourse.setText("赤")
                        binding.textViewBusCourse.setTextColor(Color.parseColor("#FF0202"))
                    }
                    3 -> {
                        binding.textViewBusCourse.setText("緑")
                        binding.textViewBusCourse.setTextColor(Color.parseColor("#00cc00"))
                    }
                    4 -> {
                        binding.textViewBusCourse.setText("Ａ")
                        binding.textViewBusCourse.setTextColor(Color.parseColor("#1919FF"))
                    }
                    5 -> {
                        binding.textViewBusCourse.setText("Ｂ")
                        binding.textViewBusCourse.setTextColor(Color.parseColor("#FF0202"))
                    }
                    6 -> {
                        binding.textViewBusCourse.setText("Ｃ")
                        binding.textViewBusCourse.setTextColor(Color.parseColor("#00cc00"))
                    }
                    else -> {
                        println("else")
                    }
                }
                // 名前
                val fullName = kids.lastName + "　" + kids.firstName
                binding.textViewName.text = fullName

                // クラス
                binding.textViewClass.text = kids.kClass
                // 学年色を変える
                val k_year = kids.kYear
                when (k_year) {
                    9 -> {
                        binding.imageViewHana.setImageResource(R.drawable.ic_baseline_local_florist_24_green)
                    }
                    6 -> {
                        binding.imageViewHana.setImageResource(R.drawable.ic_baseline_local_florist_24_yellow)
                    }
                    7 -> {
                        binding.imageViewHana.setImageResource(R.drawable.ic_baseline_local_florist_24_blue)
                    }
                    8 -> {
                        binding.imageViewHana.setImageResource(R.drawable.ic_baseline_local_florist_24_red)
                    }
                    else -> {
                        println("else")
                    }
                }


//                this.viewModel = viewModel
                executePendingBindings()
            }
        }
    }

    // WordsComparator は、2 つの単語が同じかどうか、またはコンテンツが同じかどうかを計算する方法を定義します
    class WordsComparator : DiffUtil.ItemCallback<RideKidsView>() {
        override fun areItemsTheSame(oldItem: RideKidsView, newItem: RideKidsView): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: RideKidsView, newItem: RideKidsView): Boolean {
            return oldItem.rideViewId == newItem.rideViewId
        }
    }
}